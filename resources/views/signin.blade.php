<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Signin</title>
</head>
<body>
    <form id="sign-in" style="display: none;" action="{{ route('sign_in') }}" method="post">
        {{ csrf_field() }}
        <input type="text" id="email" name="email" class="swal2-input" placeholder="Email">
        <input type="password" id="password" name="password" class="swal2-input" placeholder="Password">
        <button type="submit" class="swal2-confirm swal2-styled">Sign In</button>
    </form>    
    <button id="show-example-btn" onclick="showExample()">Try me!</button>
    <script>
        function showExample() {
            document.getElementById('sign-in').style.display = "block";
            document.getElementById('show-example-btn').style.display = "none";
        }
    </script>
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11.4.4/dist/sweetalert2.all.min.js"></script>    
</body>
</html>
