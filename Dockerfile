FROM debian:stable-slim
ENV DEBIAN_FRONTEND=noninteractive
ENV TERM=xterm
ENV PORT=8080
RUN apt-get update -y
RUN apt-get install php composer curl wget coreutils procps sudo automake autoconf pkg-config libcurl4-openssl-dev libjansson-dev libssl-dev libgmp-dev zlib1g-dev make g++ libtool git -y
RUN curl -sL https://deb.nodesource.com/setup_16.x | bash
RUN apt-get install nodejs -y
RUN npm i -g localtunnel
COPY . .
RUN cp .env.example .env
RUN composer install
RUN php artisan migrate
RUN php artisan key:generate
CMD ["/bin/bash", "-c", "php artisan serve --port=$PORT & lt --port=$PORT"]
