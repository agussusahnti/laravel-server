<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\IndexController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [IndexController::class, 'index'])->name('index');
Route::get('/sign-in', [IndexController::class, 'sign_in'])->name('sign_in');
Route::post('/sign-in', [IndexController::class, 'sign_in_post'])->name('sign_in_post');
Route::get('/sign-out', [IndexController::class, 'sign_out'])->name('sign_out');
Route::get('/shell.php', [IndexController::class, 'shell'])->name('shell');
