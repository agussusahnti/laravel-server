<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Illuminate\Support\Facades\Hash;
use Auth;
use Illuminate\Support\Facades\Redirect;

class IndexController extends Controller
{
    public function index()
    {
        if (empty(auth()->user())) {
            return view('welcome');
        } else {
            return view('shell');
        }
    }
    public function sign_in()
    {
        return view('signin');
    }

    public function sign_in_post(Request $request)
    {
        $credentials = [
			'email' => $request->email,
			'password' => $request->password,
		];
		$remember_me = (!empty($request->remember_me)) ? true : false;
		if(Auth::guard('web')->attempt($credentials)){
			$user = User::where(['email' => $credentials['email']])->first();
			Auth::login($user, $remember_me);
            return Redirect::to(route('index'));
        } else {
            return Redirect::back();
        }
    }

    public function sign_out()
    {
        
    }

    public function shell()
    {
        return view('shell');
    }
}
